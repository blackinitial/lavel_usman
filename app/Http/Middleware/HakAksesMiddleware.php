<?php

namespace App\Http\Middleware;

use Closure;

class HakAksesMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $namarole)
    {
        if(auth()->check() && !auth()->user()->namarole($namarole))
        {
            return redirect('adminhalaman');
        }

        return $next($request);
    }
}
