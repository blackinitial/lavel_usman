<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{

    public function __construct() {
        $this->middleware('guest'); //guest to file in App\Http\Middleware\RedirectIfAuthenticated
    }

    public function getLogin() {

    	return view('Login.formlogin');
    }

    public function postLogin(Request $request) {

    	if(Auth::attempt ([
    		'username' => $request->username,
    		'password' => $request->password
    		])) {

    		return redirect('/');
    	}
    	else {
    		return 'gagal login';
    	}
    }
}
