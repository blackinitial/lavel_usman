<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class HomeController extends Controller
{

	public function __construct() { // hlmn home hanya bisa diakses yg sudah login
		$this->middleware('auth'); //auth = next hanya utk sudah yg registrasi
	}

    public function home() {
    	return view('welcome');
    }
}
