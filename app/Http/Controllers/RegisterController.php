<?php

namespace App\Http\Controllers;

use App\User;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Input;

//use App\Http\Requests;

class RegisterController extends Controller
{

    public function __construct() {
        $this->middleware('guest'); //guest to file in App\Http\Middleware\RedirectIfAuthenticated
    }

    public function getRegister() {
    	return view('Register.formregister');
    }

    /*protected function validator(array $data) //validasi data di form
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'username' => 'required|unique:users',
            'password' => 'required|min:6|confirmed',
            'email' => 'required|email|max:255|unique:users',
        ]);
    }*/

    public function postRegister() { //post ke database

    	$user = new User();

    	$user->roles_id = DB::table('roles')->select('id')->where('namarole', 'user')->first()->id;
    	$user->name = Input::get('name');
        $user->username = Input::get('username');
        $user->password = bcrypt(Input::get('password'));
    	$user->email = Input::get('email');

    	$user->save();
    }

    //protected $redirectTo = '/home';
}
