<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'HomeController@home');

Route::get('/register', 'RegisterController@getRegister');
Route::post('postregister', 'RegisterController@postRegister');

Route::get('/login', 'LoginController@getLogin');
Route::post('postlogin', 'LoginController@postLogin');

Route::get('/logout', function() {
	Auth::logout();
	return 'sukses logout';
});


Route::get('/adminhome', function() {
	return view ('Layouts.adminhome');
});
Route::get('/home', function() {
	return view ('Layouts.home');
});
Route::get('adminhalaman', function() {
	return view('adminhalaman');
});

Route::get('delete', 'AdminController@delete');
Route::get('update', 'AdminController@update');

